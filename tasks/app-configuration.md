# App-Configuration

- In your personal namespace adjust the deployment for your nginx. It should receive a new nginx.conf file which simply serves a hello-world.html file
    - Hint: can you solve this with only one ConfigMap (cm) ressource?
- Add a second deployment named nginx-with-replicas in your namespace that runs nginx with 4 replicas and an update strategy defined as RollingUpdate
    - The deployment should have the following labels (and corresponding required selectors) – it’s up to you how you label the ressources, there are several possible solutions/ways
        - lab: app-configuration, part-of: sr-k8s-training, app: nginx-with-replicas
        - BONUS: add a label track: blue to the deployment and its pods
    - Expose the deployment with an Service (svc) that has ClusterIP type and selectorLabels for app: nginx-with-replicas labelled pods


