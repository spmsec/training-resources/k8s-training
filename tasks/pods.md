# Lab: Pods

- In your personal namespace
    - Imperatively start a pod that runs a nginx image and is named imperative-nginx
    - Create and apply a pod manifest that will create a pod with name yaml-nginx with a nginx image 
    - Start a pod that runs an nginx:latest image and is named example-nginx. You can choose between starting the pods imperatively or creating a manifest file beforehand
- Retrieve the yaml manifest for the example-app pod in the default namespace
    - Apply it in your personal namespace and in a namespace called <participant_id>-busybox
    - In the namespace <participant_id>-busybox edit the pods image to busybox
    - Find out why the Pod broken-pod in the default namespace is failing and apply the fix in your personal namespace
