In your personal namespace:

- Duplicate the deployed example-app-with-svc into your personal namespace (you will find the pod by the label hiddem: gen)
    - Create a kubernetes manifest, that defines a service for the example-app-with-svc. The Service should have a selector for the label app: example-app. Ensure the example-app pod has the corresponding label attached (kubectl --help might help).
- Imperatively, create a service resource for the yaml-nginx pod, with a service type set to ClusterIP. (kubectl create svc --help might be a good starting point)

> Test if everything works by doing a port-forward for the service port to local port 1234 and access localhost:1234 in your browser
