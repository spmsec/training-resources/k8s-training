# Lab: Explore the Cluster

- Find out the amount of nodes in the cluster
    - How many of them in which plane?
    - How many of the nodes can accept workloads and why?
- Retrieve all listed information about the cluster:
    - Available Namespaces
    - Available Pods
    - Available Services
- In the namespace default, you can find several applications deployed:
    -   Find out which container image is used to run the example-app in the default namespace
 

Hint: For each task, you should be able to explain what you did and how you solved it. If it helps you, it might be useful to store the results of some operations in a file.