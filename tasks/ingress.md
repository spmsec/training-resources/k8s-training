# Lab: Ingress

In the cluster:
- Find out how many ingress resources are available throughout all namespaces
- For the example-app, create an ingress resource that is named example-ingress, and defines a rule that routes all http requests for the path prefix “/” to the defined example-service on port 80.
    - create a manifest file named “example-ingress-manifest.yaml” for your solution
    - Create one manifest file that contains the following Kubernetes objects: Pod (use nginx image), Service, Ingress*
- In the namespace default you will find a broken deployment. Fix it, then Imperatively add service and ingress resources for the pod.
    - Add a label stage: qa to the pod which should be the selector for the service
- Find out how many ingress resources are available throughout all namespaces

*the hostname should follow the pattern: `<personal_id>-public-demo-app.k8s.spsw-trainings.dev`
